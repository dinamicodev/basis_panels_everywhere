<?php
/**
 * @file
 * basis_panels_everywhere.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function basis_panels_everywhere_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'basis_page_layout';
  $handler->task = 'site_template';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = -10;
  $handler->conf = array(
    'title' => 'Basis Default',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'main',
  );
  $display = new panels_display();
  $display->layout = 'basis_site_template';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => array(
        'clean_markup' => array(
          'region_wrapper' => 'header',
          'additional_region_classes' => 'header',
          'additional_region_attributes' => 'role="banner"',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'main' => array(
        'clean_markup' => array(
          'region_wrapper' => 'div',
          'additional_region_classes' => 'main',
          'additional_region_attributes' => 'role="main"',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
      'footer' => array(
        'clean_markup' => array(
          'region_wrapper' => 'footer',
          'additional_region_classes' => 'footer',
          'additional_region_attributes' => '',
          'enable_inner_div' => 0,
          'pane_separators' => 0,
        ),
      ),
    ),
    'header' => array(
      'style' => 'clean_element',
    ),
    'main' => array(
      'style' => 'clean_element',
    ),
    'footer' => array(
      'style' => 'clean_element',
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'd8f7c0fa-975e-4d85-9a66-092fe973f675';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-e536ca2e-de88-4458-b165-9b34fe06e8f1';
  $pane->panel = 'header';
  $pane->type = 'pane_header';
  $pane->subtype = 'pane_header';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'naked',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'e536ca2e-de88-4458-b165-9b34fe06e8f1';
  $display->content['new-e536ca2e-de88-4458-b165-9b34fe06e8f1'] = $pane;
  $display->panels['header'][0] = 'new-e536ca2e-de88-4458-b165-9b34fe06e8f1';
  $pane = new stdClass();
  $pane->pid = 'new-1894c833-f655-491a-97e5-eec0b1321ab5';
  $pane->panel = 'header';
  $pane->type = 'menu_tree';
  $pane->subtype = 'main-menu';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'parent' => 'main-menu:0',
    'title_link' => 0,
    'admin_title' => 'Main navigation',
    'level' => '1',
    'follow' => 0,
    'depth' => '1',
    'depth_relative' => 0,
    'expanded' => 1,
    'sort' => 0,
    'menu_name' => 'main-menu',
    'parent_mlid' => '0',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => array(
      'clean_markup' => array(
        'pane_wrapper' => 'nav',
        'exclude_default_pane_classes' => 1,
        'include_pane_css_class' => 1,
        'additional_pane_classes' => '',
        'include_pane_css_id' => 1,
        'additional_pane_attributes' => 'role="navigation"',
        'enable_inner_div' => 0,
        'title_wrapper' => 'h2',
        'title_hide' => 1,
        'content_wrapper' => 'none',
      ),
    ),
    'style' => 'clean_element',
  );
  $pane->css = array(
    'css_id' => '',
    'css_class' => 'main-navigation',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '1894c833-f655-491a-97e5-eec0b1321ab5';
  $display->content['new-1894c833-f655-491a-97e5-eec0b1321ab5'] = $pane;
  $display->panels['header'][1] = 'new-1894c833-f655-491a-97e5-eec0b1321ab5';
  $pane = new stdClass();
  $pane->pid = 'new-b1f97a29-3a8b-4a16-bf74-0eb9618124ee';
  $pane->panel = 'main';
  $pane->type = 'pane_messages';
  $pane->subtype = 'pane_messages';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array();
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'b1f97a29-3a8b-4a16-bf74-0eb9618124ee';
  $display->content['new-b1f97a29-3a8b-4a16-bf74-0eb9618124ee'] = $pane;
  $display->panels['main'][0] = 'new-b1f97a29-3a8b-4a16-bf74-0eb9618124ee';
  $pane = new stdClass();
  $pane->pid = 'new-a78825a0-96af-4f86-b45c-60955a72a632';
  $pane->panel = 'main';
  $pane->type = 'page_content';
  $pane->subtype = 'page_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => 'argument_page_content_1',
    'override_title' => 1,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'naked',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'a78825a0-96af-4f86-b45c-60955a72a632';
  $display->content['new-a78825a0-96af-4f86-b45c-60955a72a632'] = $pane;
  $display->panels['main'][1] = 'new-a78825a0-96af-4f86-b45c-60955a72a632';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['basis_page_layout'] = $handler;

  return $export;
}
