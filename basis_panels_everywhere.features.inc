<?php
/**
 * @file
 * basis_panels_everywhere.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function basis_panels_everywhere_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
