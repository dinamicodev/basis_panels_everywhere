<?php
/**
 * @file
 * Template for the Basis Page layout.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
#dpm($content);
// kpr(get_defined_vars());
// dpm($)

/* <div<?php print $attributes ?>>*/
/* <div class="panel-display <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>> */
?>

<div class="page" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <?php if ($use_default_wrapper['header'] && !empty($content['header'])): ?>
  <header class="header">
    <?php print $content['header']; ?>
  </header>
  <?php elseif (!empty($content['header'])): ?> 
    <?php print $content['header']; ?>
  <?php endif; ?>

  <?php if ($use_default_wrapper['navigation'] && !empty($content['navigation'])): ?>
  <div class="main-navigation">
    <?php print $content['navigation']; ?>
  </div>
  <?php elseif (!empty($content['navigation'])): ?> 
    <?php print $content['navigation']; ?>
  <?php endif; ?>

  <main>

    <?php if ($use_default_wrapper['main'] && !empty($content['main'])): ?>
    <div class="main-content">
      <?php print $content['main']; ?>
    </div>
    <?php elseif (!empty($content['main'])): ?> 
      <?php print $content['main']; ?>
    <?php endif; ?>

    <?php if ($use_default_wrapper['content'] && !empty($content['content'])): ?>
    <div class="main-content">
      <?php print $content['content']; ?>
    </div>
    <?php elseif (!empty($content['content'])): ?> 
      <?php print $content['content']; ?>
    <?php endif; ?>

    <?php if ($use_default_wrapper['sidebar_first'] && !empty($content['sidebar_first'])): ?>
    <aside class="sidebar sidebar--first">
      <?php print $content['sidebar_first']; ?>
    </aside>
    <?php elseif (!empty($content['sidebar_first'])): ?> 
      <?php print $content['sidebar_first']; ?>
    <?php endif; ?>

    <?php if ($use_default_wrapper['sidebar_second'] && !empty($content['sidebar_second'])): ?>
    <aside class="sidebar sidebar--second">
      <?php print $content['sidebar_second']; ?>
    </aside>
    <?php elseif (!empty($content['sidebar_second'])): ?> 
      <?php print $content['sidebar_second']; ?>
    <?php endif; ?>

  </main>

  <?php if ($use_default_wrapper['footer'] && !empty($content['footer'])): ?>
  <footer class="footer">
    <?php print $content['footer']; ?>
  </footer>
  <?php elseif (!empty($content['footer'])): ?> 
    <?php print $content['footer']; ?>
  <?php endif; ?>

</div>
