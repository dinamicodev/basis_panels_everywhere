<?php
/**
 * @file
 * Definition of Page layout.
 */

$plugin = array(
  'title' => t('Basis Site Template'),
  'theme' => 'basis_site_template',
  'icon' => 'basis_site_template.png',
  'category' => 'Basis',
  'regions' => array(
    'header' => t('Header'),
    'navigation' => t('Navigation'),
    'content' => t('Content'),
    // @todo Remove main in next version. Content is the new region for main
    //   content.
    'main' => t('Main'),
    'sidebar_first' => t('First sidebar'),
    'sidebar_second' => t('Second sidebar'),
    'footer' => t('Footer'),
  ),
);

/**
 * Preprocess variables for zen-main.tpl.php.
 *
 * Because of Panels' inside-out rendering method we can only detect empty
 * sidebars at this stage. We then set a static variable so that we can add
 * the appropriate body classes later.
 */
function basis_panels_everywhere_preprocess_basis_site_template(&$variables) {
  // dargs();
  $panel_settings = $variables['display']->panel_settings;
  $default_style_type = $panel_settings['style'];

  foreach ($variables['layout']['regions'] as $region => $region_name) {
    if (
      (isset($panel_settings[$region]['style']) && $panel_settings[$region]['style'] == 'default') || 
      (isset($panel_settings[$region]['style']) && $panel_settings[$region]['style'] == '-1' && $default_style_type == 'default') ||
      (!isset($panel_settings[$region]['style']) && $default_style_type == 'default')
      ) {
      $variables['use_default_wrapper'][$region] = TRUE;
    } else {
      $variables['use_default_wrapper'][$region] = FALSE;
    }
  }

  // Put any body classes we want into this static variable.
  $classes = &drupal_static('basis_panels_everywhere_classes_array', array());

  if (!empty($variables['content']['sidebar_first']) && !empty($variables['content']['sidebar_second'])) {
    $classes[] = 'two-sidebars';
  }
  elseif (!empty($variables['content']['sidebar_first'])) {
    $classes[] = 'one-sidebar sidebar-first';
  }
  elseif (!empty($variables['content']['sidebar_second'])) {
    $classes[] = 'one-sidebar sidebar-second';
  }
  else {
    $classes[] = 'no-sidebars';
  }
}
